import React, { useState, useEffect, useContext } from "react";
import PageTemplate from '../components/templateMovieListPage'
import TextField from "@mui/material/TextField";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { useParams, Link } from 'react-router-dom';
import { getMovies } from "../api/tmdb-api";
import { useQuery } from 'react-query';
import Spinner from '../components/spinner';
import AddToFavoritesIcon from '../components/cardIcons/addToFavourites'

const HomePage = (props) => {
  const [movies, setMovies] = useState([]);
  const {  data, error, isLoading, isError }  = useQuery('discover', getMovies)
  const { id } = useParams();
  const [page, setPage] = React.useState(1);
  const [searchTerm, setSearchTerm] = useState("")

  const handleChange = (event, value) => {
    setPage(value);
    console.log(value)
  };

  useEffect(() => {
    getMovies().then(movies => {
      setMovies(movies);
    });
  }, [id]);

  if (isLoading) {
    return <Spinner />
  }

  if (isError) {
    return <h1>{error.message}</h1>
  }  

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value)
  }

  const onSubmit = async (e) => {
    e.preventDefault()
    let slug = searchTerm.split(' ').join('-').toLowerCase()
    const url = `https://api.themoviedb.org/3/search/movie?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&include_adult=false&page=1&query=${slug}`;
    const data = await fetch(url);
    const movies = await data.json();
    setMovies(movies.results);
    console.log(movies.results)
}

const pagination = async () => {
  const data = await fetch( `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&include_adult=false&page=${page}`);
  const movies = await data.json();
  setMovies(movies?.results);
};

  return (
    <div className="movies">
    <form onSubmit={onSubmit}>
    <TextField
    id="filled-search"
    fullWidth 
    label="Search for a movie"
    type="searchTMDB"
    variant="filled"
    value={searchTerm}
    onChange={handleSearchChange}
        />
        <br></br>
        </form>

        <PageTemplate
        title="Discover Movies"
        movies={movies}
        action={(movie) => {
          return <AddToFavoritesIcon movie={movie} />
        }}
      />

<Link to={`/movie/${page}`}>
    <Stack spacing={2}>
      <Pagination variant="outlined" shape="rounded" showFirstButton showLastButton count={100} page={page} onChange={handleChange} onClick={pagination} />
    </Stack>
    </Link>
    </div>
  );
};
export default HomePage;